def call(String url) {

    stage('Clone'){
        scmVars = checkout([$class: 'GitSCM',userRemoteConfigs: [[url: "$url"]]])
    }

node {
    
    stage('Install') { 
       
        'sudo -u apt-get update'
        'sudo -u apt-get install -y mongodb-org'
        'sudo -u systemctl start mongod.service'
   }
   
   stage('Stop') {
        'sudo -h systemctl stop mongod.service'
        'sudo -h systemctl status mongod.service'
}
   stage('Start') {
        'sudo -h systemctl start mongod.service'
        'sudo -h systemctl status mongod.service'
}
   stage('Status') {
        'sudo -h systemctl status mongod.service'

}
   stage('Comment_security') {
        "sudo -h sed -i '/security/,/keyFile/ s/^/# /' /etc/mongod.conf"


}
   stage('Download_data') {
          'sudo -h curl -O https://www.cyberciti.biz/files/sticker/sticker_book.pdf'
}
   stage('Extract_data') {
        'sudo -h tar -xvf $file'
}
   stage('Move_data') {
     'sudo -h mv $file /var/lib/mongodb/'
     'sudo -h chown -R mongod:mongod mongodb'

}
}
